class SystemAdminsController < ApplicationController
  # GET /system_admins
  # GET /system_admins.json
  def index
    @system_admins = SystemAdmin.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @system_admins }
    end
  end

  # GET /system_admins/1
  # GET /system_admins/1.json
  def show
    @system_admin = SystemAdmin.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @system_admin }
    end
  end

  # GET /system_admins/new
  # GET /system_admins/new.json
  def new
    @system_admin = SystemAdmin.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @system_admin }
    end
  end

  # GET /system_admins/1/edit
  def edit
    @system_admin = SystemAdmin.find(params[:id])
  end

  # POST /system_admins
  # POST /system_admins.json
  def create
    @system_admin = SystemAdmin.new(params[:system_admin])

    respond_to do |format|
      if @system_admin.save
        format.html { redirect_to @system_admin, notice: 'System admin was successfully created.' }
        format.json { render json: @system_admin, status: :created, location: @system_admin }
      else
        format.html { render action: "new" }
        format.json { render json: @system_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /system_admins/1
  # PUT /system_admins/1.json
  def update
    @system_admin = SystemAdmin.find(params[:id])

    respond_to do |format|
      if @system_admin.update_attributes(params[:system_admin])
        format.html { redirect_to @system_admin, notice: 'System admin was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @system_admin.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /system_admins/1
  # DELETE /system_admins/1.json
  def destroy
    @system_admin = SystemAdmin.find(params[:id])
    @system_admin.destroy

    respond_to do |format|
      format.html { redirect_to system_admins_url }
      format.json { head :no_content }
    end
  end
end

class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :name
      t.datetime :date
      t.string :reason

      t.timestamps
    end
  end
end

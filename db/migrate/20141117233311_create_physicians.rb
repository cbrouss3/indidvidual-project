class CreatePhysicians < ActiveRecord::Migration
  def change
    create_table :physicians do |t|
      t.string :name
      t.string :notes
      t.string :code

      t.timestamps
    end
  end
end

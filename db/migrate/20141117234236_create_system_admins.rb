class CreateSystemAdmins < ActiveRecord::Migration
  def change
    create_table :system_admins do |t|
      t.string :name

      t.timestamps
    end
  end
end
